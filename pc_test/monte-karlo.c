#include <stdio.h>
#include <mpi.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>

double generateRand() {
	double r = rand() % 4;
	double ri = rand() / 10000000;
	return r + ri / 1000;
}

float f2(float x) {
	return x;
}

struct Point {
	float x, y;
};

double f1(struct Point p) {
	// printf("f(%lf, %lf) = %lf\n", p.x, p.y, pow(p.x * p.y, -2));
	return pow(p.x * p.y, -2);
}

short test_point(struct Point p) {
	return 1;
}

double simpleMonteKarlo(
	struct Point *points, 
	int n, 
	double (*f) (struct Point p),
	double maxX,
	double maxY,
	double minX,
	double minY
	) {
	double val = 0;
	for (int i = 0; i < n; i++) {
		val += f(points[i]);
		// printf("f(%lf, %lf) = %lf\n", points[i].x, points[i].y, f(points[i]));
	}
	printf("range = %lf\n", (((3.0 - 0) * (3.0 - 0)) / n));
	printf("sum = %lf\n", val);
	return (((3 - 0) * (3 - 0))) * val / n;
}

double geometrMonteKarlo(
	struct Point *points, 
	int n
) {
	int range = 1;
	double line1;
	double line2;
	double line3;
	double line4;
	for (int i = 0; i < n; i++) {
		line1 = (0.75 - points[i].x) * (1.5 - 0.25) - (0.5 - 0.75) * (0.25 - points[i].y);
		line2 = (0.5 - points[i].x) * (3 - 1.5) - (1 - 0.5) * (1.5 - points[i].y);
		line3 = (1 - points[i].x) * (1 - 3) - (3 - 1) * (3 - points[i].y);
		line4 = (3 - points[i].x) * (0.25 - 1) - (0.75 - 3) * (1 - points[i].y);
		if (line1 > 0 && line2 > 0 && line3 < 0 && line4 < 0) {
			range++;
		}
	}
	range = n / range;
	return range / 100.0;
}


double Rmk(int n) {
	return sqrt(1 / n);
}

int main(int argc, char *argv[])
{
	srand(time(NULL));
	int ProcNum, ProcRank, RecvRank;
	MPI_Status Status;

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &ProcNum);
	MPI_Comm_rank(MPI_COMM_WORLD, &ProcRank);
	const int n = 10000;
	const double maxX = 3;
	const double maxY = 3;
	const double minX = 0.5;
	const double minY = 0.25;
	int newN = 0;
	struct Point *points1 = malloc(sizeof(struct Point) * n);
	struct Point *points2 = malloc(sizeof(struct Point) * n);
	for (int i = 0; i < n; i++) {
		if (ProcRank == 0) {
			double temX = generateRand();
			double temY = generateRand();
			points2[i].y = temY;
			points2[i].x = temX;
			if (temX < maxX && temX > minX && temY < maxY && temY > minY) {
				points1[newN].y = temY;
				points1[newN].x = temX;
				// printf("point[%d] x=%lf\ty=%lf\n", newN, points[newN].x, points[newN].y);
				newN++;
			}
		}
	}
	points1 = realloc(points1, newN);
	if (ProcRank == 0) {
		printf("\nJ = %lf\n", simpleMonteKarlo(points1, newN, f1, maxX, maxY, minX, minY));
		printf("\nJg = %lf\n", 25 * geometrMonteKarlo(points2, n));
		printf("\nRmk = %lf\n", Rmk(newN));
	}

	MPI_Finalize();
	free(points1);
	free(points2);
	return 0; 
}