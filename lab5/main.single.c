#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <math.h>

/**
 * Даны два множкства точек на плоскости. 
 * Определить точку второго множества точек,
 * расстояние от которой до центра масс первого минимально
*/

struct Point {
    float x, y, m;
};

struct SetOfPoints {
    struct Point* points;
    struct Point centerOfMass;
    long size;
    char name[100];
};

float segmentLen(struct Point p1, struct Point p2) {
    return sqrt(pow(p2.x - p1.x, 2) + pow(p2.y - p1.y, 2));
}

void randomizeSetOfPoints(struct SetOfPoints* set) {
    for (int i = 0; i < set->size; i++) {
        set->points[i].x = rand() / 10000000;
        set->points[i].y = rand() / 10000000;
        set->points[i].m = 1;
    }
}

struct SetOfPoints* newSetOfPoints(char * name, int size, short randomize) {
    struct SetOfPoints* set = malloc(sizeof(struct SetOfPoints));
    set->points = malloc(sizeof(struct Point) * size);
    set->size = size;
    strcpy(set->name, name);
    set->centerOfMass.x = 0;
    set->centerOfMass.y = 0;
    if (randomize)
        randomizeSetOfPoints(set);
    return set;
}

void freeSetOfPoints(struct SetOfPoints *set) {
    free(set->points);
    free(set);
}

void printSetOfPoints(struct SetOfPoints* set) {
    printf("\nSet of points %s\n", set->name);
    printf("Center of Mass x=%f\ty=%f\n", set->centerOfMass.x, set->centerOfMass.y);
    for (int i = 0; i < set->size; i++) {
        printf("\tPoint Num=%d\tx=%f\ty=%f\tm=%f\n", i+1, set->points[i].x, set->points[i].y, set->points[i].m);
    }
}

void countCenterOfMass(struct SetOfPoints* set) {
    int summ_M = 0;
    for (int i = 0; i < set->size; i++) {
        set->centerOfMass.x += set->points[i].x * set->points[i].m;
        set->centerOfMass.y += set->points[i].y * set->points[i].m;
        summ_M += set->points[i].m;
    }
    set->centerOfMass.x /= summ_M;
    set->centerOfMass.y /= summ_M;
}

int main(int argc, char* argv[]) {
    //Need to get time metrics//
    const long long startTime = clock();
    // srand(time(NULL));
    /**
     * ===============Programm Body===============
    */

    //Declare vars//
    struct SetOfPoints* set1; 
    struct SetOfPoints* set2;
    long countOfPoints = atol(argv[1]);
    printf("\nCount of elements: %lu\n", countOfPoints);
    //End Declare vars//

    set1 = newSetOfPoints("Set1", countOfPoints, 1);
    set2 = newSetOfPoints("Set2", countOfPoints, 1);

    countCenterOfMass(set1);
    // printSetOfPoints(set1);
    // printSetOfPoints(set2);

    int pointer = 0;
    struct Point p = set2->points[pointer];
    float segmentLen1 = 0, segmentLen2 = 0;

    for (int i = 1; i < set2->size; i++) {
        segmentLen1 = segmentLen(set1->centerOfMass, set2->points[i]); 
        if (segmentLen1 < segmentLen2) {
            p = set2->points[i];
            pointer = i;
            segmentLen2 = segmentLen(set1->centerOfMass, p);
        }
    }

    printf("Result: Point x=%f\ty=%f\tm=%f", p.x, p.y, p.m);

    //Free space//
    freeSetOfPoints(set1);
    freeSetOfPoints(set2);

    /**
     * ===============End Programm Body===============
    */
    //Need to get time metrics//
    const long long endTime = clock();
    printf("\nSpended time %lld ms.\n", endTime - startTime);
    return 0;
} 
