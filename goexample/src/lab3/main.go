package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"sync"
	"time"
)

func main() {

	startTime := time.Now().Nanosecond()

	if len(os.Args) < 2 {
		fmt.Println("Need file to start a workin!")
		return
	}

	src := os.Args[1]

	fmt.Println("File ", src)

	//Открываем файл для чтения
	file, err := os.Open(src)
	if err != nil {
		fmt.Println("Invalid filepath or file does not exists!")
		return
	}
	//отложенное закрытие файла, после того как отработает функция
	defer file.Close()

	//Получаем информацию о файле
	stat, err := file.Stat()
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	//создаем сканнер для чтения из файла по строкам
	resource := bufio.NewScanner(file)

	//WaitGroup - нужна для многопоточного контроля за колличеством обрабатываемых строк
	var runningProc sync.WaitGroup
	const WorkerCount = 10

	fmt.Println("File size", stat.Size(), "b")

	//Создаем каналы для общения потоков
	procTube := make(chan string, 1000)
	defer close(procTube)
	resultTube := make(chan string, 1000)
	result := make(chan string)
	defer close(result)
	quit := make(chan byte)

	//Запускаем определенное количество зеленых потоков для нахождения самого большого слова
	for i := 0; i < WorkerCount; i++ {
		go func() {
			parser := []string{",", "?", ";", "!", ".", ":", "\""}
			for {
				select {
				case str := <-procTube:
					for _, s := range parser {
						str = strings.Replace(str, s, "", -1)
					}
					words := strings.Split(str, " ")
					bWord := ""
					for _, w := range words {
						if len(w) > len(bWord) {
							bWord = w
						}
					}
					resultTube <- bWord
					break
				case <-quit:
					return
				}
			}
		}()
	}

	//Запускаем в отдельном потоке обработку результатов от функции нахождения
	//максимального слова для нахождения наибольшего слова
	go func() {
		bWord := ""
		for w := range resultTube {
			if len(w) > len(bWord) {
				bWord = w
			}
			runningProc.Done()
		}
		result <- bWord
	}()

	fmt.Println("----------------BEGIN READING TEXT----------------")
	for resource.Scan() {
		line := resource.Text()
		if err := resource.Err(); err != nil {
			fmt.Println(err.Error())
			break;
		}
		fmt.Println(line)
		procTube <- line
		runningProc.Add(1)
	}
	fmt.Println("----------------END READING TEXT----------------")
	//Ждем пока не обработаются все строки
	runningProc.Wait()
	fmt.Println("Workint time", float64(time.Now().Nanosecond() - startTime) / 1000000, "ms")
	//Посылаем сигнал для закрытия воркеров
	for i := 0; i < WorkerCount; i++ {
		var b byte
		quit <- b
	}
	//Закрываем каналы - если закрыты каналы, то завершатся потоки, которые их используют
	close(resultTube)
	close(quit)

	//получаем результат работы всего - наше самое большое слово
	bWord := <-result
	fmt.Println("Самое большое слово", bWord, "его длина", len(bWord), "смв.")
}
