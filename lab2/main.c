
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <mpi.h>

//allocArray init array by rand and return pointer to the memory block
int * allocArray(int size) {
	srand(time(NULL));
	int *buffer = malloc(sizeof(int)*size);
	for (int i = 0; i< size; i++) {
		buffer[i] = rand() / 10000000;
	}
	return buffer;
}
//printArray print array
void printArray(int *ar, int size, int p_rank) {
	for (int i = 0; i < size; i++) {
		printf("Process[%d]\tarray[%d] %d\n", p_rank, i, ar[i]);
	}
}

int SPV(int *a, int *b, int size) {
	int spv = 0;
	for (int i = 0; i< size; i++)
		spv += a[i]*b[i];
	return spv;
}

int main(int argc, char* argv[])
{   
	int ProcNum, ProcRank, RecvRank, arraySize, *array, *arrayBuffer;
	MPI_Request req, req1;
	MPI_Status status;
	int dest = 0;
	int src = 0;
	MPI_Init(&argc, &argv);    
	MPI_Comm_size(MPI_COMM_WORLD, &ProcNum);  
	MPI_Comm_rank(MPI_COMM_WORLD, &ProcRank);
	arraySize = atoi(argv[1]);
	if (ProcRank < 3)
		array = allocArray(arraySize);
	switch(ProcRank) {
		//A*B
		case 0:
			MPI_Isend(array, arraySize, MPI_INT, 2, 0, MPI_COMM_WORLD, &req);
			arrayBuffer = malloc(sizeof(int)*arraySize);

			MPI_Irecv(arrayBuffer, arraySize, MPI_INT, 1, MPI_ANY_TAG, MPI_COMM_WORLD, &req1);

			int SPVs[3];
			MPI_Request SPVReq[2];

			MPI_Irecv(&SPVs[1], 1, MPI_INT, 1, MPI_ANY_TAG, MPI_COMM_WORLD, &SPVReq[0]);
			MPI_Irecv(&SPVs[2], 1, MPI_INT, 2, MPI_ANY_TAG, MPI_COMM_WORLD, &SPVReq[1]);
			MPI_Wait(&req1, MPI_STATUSES_IGNORE);

			SPVs[0] = SPV(array, arrayBuffer, arraySize);

			MPI_Waitall(2, SPVReq, MPI_STATUSES_IGNORE);

			printf("SPV %d\n", SPVs[1] - SPVs[0] + SPVs[2]);
			free(arrayBuffer);
			break;
		//B*C MPI_Isend=0	MPI_Recv=2
		case 1:
		//A*C MPI_Isend=1	MPI_Recv=0
		case 2:
			if (ProcRank == 2) {
				dest = 1;
				src = 0;
			} else {
				dest = 0;
				src = 2;
			}
			arrayBuffer = malloc(sizeof(int)*arraySize);
			MPI_Isend(array, arraySize, MPI_INT, dest, 0, MPI_COMM_WORLD, &req);
			MPI_Recv(arrayBuffer, arraySize, MPI_INT, src, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
			int spv = SPV(array, arrayBuffer, arraySize);
			MPI_Send(&spv, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
			free(arrayBuffer);
			MPI_Wait(&req, MPI_STATUSES_IGNORE);
			break;
		default:
			break;
	}
	free(array);
	MPI_Finalize();   
	return 0; 
} 
