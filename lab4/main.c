
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <mpi.h>
#include <string.h>

int main(int argc, char* argv[])
{
	int ProcNum, ProcRank, RecvRank, lineCount;
	
	const size_t B_SIZE = 256;
	const size_t LINE_SIZE = 256;
	char buffer[B_SIZE][LINE_SIZE];

	MPI_Status status;
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &ProcNum);  
	MPI_Comm_rank(MPI_COMM_WORLD, &ProcRank);
	// MPI_Datatype MPI_DEF_STRING;
	// MPI_Type_contiguous(100, MPI_CHAR, &MPI_DEF_STRING);
	// MPI_Type_commit(&MPI_DEF_STRING);
	const char *SRC = argv[1];
	FILE *resource;
	switch (ProcRank) {
		case 0:
			resource = fopen(SRC, "r");
			if (resource == NULL) {
				printf("ERROR! Can`t open recource %s\n", SRC);
				break;
			}
			lineCount = 0;
			while (!feof(resource)) {
				fscanf(resource, "%[^\r\n]%*1[\r\n]", buffer[lineCount]);
				if (lineCount == B_SIZE) {
					break;
				}
				printf("\n%s\n", buffer[lineCount]);
				lineCount++;
			}
			fclose(resource);
			break;	
	}

	MPI_Finalize();
	return 0; 
} 
