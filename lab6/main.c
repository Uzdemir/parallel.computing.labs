
////////////////////////////////////////////////////////////////////////////////

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <OpenCL/opencl.h>

////////////////////////////////////////////////////////////////////////////////

// Use a static data size for simplicity
//
#define DATA_SIZE (1536)
#define MAX_SOURCE_SIZE (2048)

int main(int argc, char** argv)
{
    srand(time(0));
    int err;                            // error code returned from api calls
      
    float a[DATA_SIZE];              // original data set given to device
    float b[DATA_SIZE];              // original data set given to device
    float c[DATA_SIZE];              // original data set given to device
    unsigned int correct;               // number of correct results returned

    size_t global;                      // global domain size for our calculation
    size_t local;                       // local domain size for our calculation

    cl_device_id device_id;             // compute device id 
    cl_context context;                 // compute context
    cl_command_queue commands;          // compute command queue
    cl_program program;                 // compute program
    cl_kernel kernel;                   // compute kernel

    ///////////////////////////////////////LOAD KERNEL/////////////////////////////////////////
    FILE *fp;
	char fileName[] = "./lab6/kernel.cl";
	char *kernel_source;
	size_t kernel_source_size;

	/* Load the source code containing the kernel*/
	fp = fopen(fileName, "r");
	if (!fp) {
		fprintf(stderr, "Failed to load kernel.\n");
		exit(1);
	}
	kernel_source = (char*)malloc(MAX_SOURCE_SIZE);
	kernel_source_size = fread(kernel_source, 1, MAX_SOURCE_SIZE, fp);
	fclose(fp);
    ////////////////////////////////////////////////////////////////////////////////
    
    // Fill our data set with random float values
    //
    int i = 0;
    unsigned int count = DATA_SIZE;
    for(i = 0; i < count; i++) {
        a[i] = rand() / (float)RAND_MAX;
        b[i] = rand() / (float)RAND_MAX;
        c[i] = rand() / (float)RAND_MAX;
    }
    
    // Connect to a compute device
    //
    int gpu = 1;
    err = clGetDeviceIDs(NULL, gpu ? CL_DEVICE_TYPE_GPU : CL_DEVICE_TYPE_CPU, 1, &device_id, NULL);
    if (err != CL_SUCCESS)
    {
        printf("Error: Failed to create a device group!\n");
        return EXIT_FAILURE;
    }
  
    // Create a compute context 
    //
    context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
    if (!context)
    {
        printf("Error: Failed to create a compute context!\n");
        return EXIT_FAILURE;
    }

    // Create a command commands
    //
    commands = clCreateCommandQueue(context, device_id, 0, &err);
    if (!commands)
    {
        printf("Error: Failed to create a command commands!\n");
        return EXIT_FAILURE;
    }

    // Create the compute program from the source buffer
    //
    program = clCreateProgramWithSource(context, 1, (const char **) & kernel_source, NULL, &err);
    if (!program)
    {
        printf("Error: Failed to create compute program!\n");
        return EXIT_FAILURE;
    }

    // Build the program executable
    //
    err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
    if (err != CL_SUCCESS)
    {
        size_t len;
        char buffer[2048];

        printf("Error: Failed to build program executable!\n");
        clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
        printf("%s\n", buffer);
        exit(1);
    }

    // Create the compute kernel in the program we wish to run
    //
    kernel = clCreateKernel(program, "task_lab", &err);
    if (!kernel || err != CL_SUCCESS)
    {
        printf("Error: Failed to create compute kernel!\n");
        exit(1);
    }

    // Get the maximum work group size for executing the kernel on the device
    //
    global = count;
    // printf("CL_KERNEL_WORK_GROUP_SIZE %u", CL_KERNEL_WORK_GROUP_SIZE);
    err = clGetKernelWorkGroupInfo(kernel, device_id, CL_KERNEL_WORK_GROUP_SIZE, sizeof(local), &local, NULL);
    if (err != CL_SUCCESS)
    {
        printf("Error: Failed to retrieve kernel work group info! %d\n", err);
        exit(1);
    }

    const size_t WORK_GROUP_COUNT = (global/local);

    float d1[WORK_GROUP_COUNT];           // results returned from device
    float d2[WORK_GROUP_COUNT];           // results returned from device
    float d3[WORK_GROUP_COUNT];           // results returned from device

    // Create the input and output arrays in device memory for our calculation
    //
    cl_mem a_cl = clCreateBuffer(context,  CL_MEM_READ_ONLY,  sizeof(float) * count, NULL, NULL);
    cl_mem b_cl = clCreateBuffer(context,  CL_MEM_READ_ONLY,  sizeof(float) * count, NULL, NULL);
    cl_mem c_cl = clCreateBuffer(context,  CL_MEM_READ_ONLY,  sizeof(float) * count, NULL, NULL);
    cl_mem d1_cl = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(float) * WORK_GROUP_COUNT, NULL, NULL);
    cl_mem d2_cl = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(float) * WORK_GROUP_COUNT, NULL, NULL);
    cl_mem d3_cl = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(float) * WORK_GROUP_COUNT, NULL, NULL);
    if (
        !a_cl || 
        !b_cl || 
        !c_cl || 
        !d1_cl ||
        !d2_cl ||
        !d3_cl
    )
    {
        printf("Error: Failed to allocate device memory!\n");
        exit(1);
    }    
    
    // Write our data set into the input array in device memory 
    //
    err = clEnqueueWriteBuffer(commands, a_cl, CL_TRUE, 0, sizeof(float) * count, a, 0, NULL, NULL);
    err = clEnqueueWriteBuffer(commands, b_cl, CL_TRUE, 0, sizeof(float) * count, b, 0, NULL, NULL);
    err = clEnqueueWriteBuffer(commands, c_cl, CL_TRUE, 0, sizeof(float) * count, c, 0, NULL, NULL);
    if (err != CL_SUCCESS)
    {
        printf("Error: Failed to write to source array!\n");
        exit(1);
    }

    // Set the arguments to our compute kernel
    //
    err = 0;
    err = clSetKernelArg(kernel, 0, sizeof(cl_mem), &a_cl);
    err = clSetKernelArg(kernel, 1, sizeof(cl_mem), &b_cl);
    err = clSetKernelArg(kernel, 2, sizeof(cl_mem), &c_cl);
    err = clSetKernelArg(kernel, 3, sizeof(cl_mem), &d1_cl);
    err = clSetKernelArg(kernel, 4, sizeof(cl_mem), &d2_cl);
    err = clSetKernelArg(kernel, 5, sizeof(cl_mem), &d3_cl);
    err = clSetKernelArg(kernel, 6, sizeof(unsigned int), &count);
    if (err != CL_SUCCESS)
    {
        printf("Error: Failed to set kernel arguments! %d\n", err);
        exit(1);
    }

    err = clSetKernelArg(kernel, 7, sizeof(cl_float) * local, NULL);
    err = clSetKernelArg(kernel, 8, sizeof(cl_float) * local, NULL);
    err = clSetKernelArg(kernel, 9, sizeof(cl_float) * local, NULL);
    err = clSetKernelArg(kernel, 10, sizeof(cl_float) * local, NULL);
    err = clSetKernelArg(kernel, 11, sizeof(cl_float) * local, NULL);
    err = clSetKernelArg(kernel, 12, sizeof(cl_float) * local, NULL);
    if (err != CL_SUCCESS)
    {
        printf("Error: Failed to set kernel local arguments! %d\n", err);
        exit(1);
    }

    // Execute the kernel over the entire range of our 1d input data set
    // using the maximum number of work group items for this device
    //
    err = clEnqueueNDRangeKernel(commands, kernel, 1, NULL, &global, &local, 0, NULL, NULL);
    if (err)
    {
        printf("Error: Failed to execute kernel!\n");
        return EXIT_FAILURE;
    }

    // Wait for the command commands to get serviced before reading back results
    //
    clFinish(commands);

    // Read back the results from the device to verify the output
    //
    err = clEnqueueReadBuffer( commands, d1_cl, CL_TRUE, 0, sizeof(float) * WORK_GROUP_COUNT, &d1, 0, NULL, NULL );  
    err = clEnqueueReadBuffer( commands, d2_cl, CL_TRUE, 0, sizeof(float) * WORK_GROUP_COUNT, &d2, 0, NULL, NULL );  
    err = clEnqueueReadBuffer( commands, d3_cl, CL_TRUE, 0, sizeof(float) * WORK_GROUP_COUNT, &d3, 0, NULL, NULL );  
    if (err != CL_SUCCESS)
    {
        printf("Error: Failed to read output array! %d\n", err);
        exit(1);
    }
    
    // Validate our results
    
    correct = 0;
    float AB = 0, BC = 0, AC = 0, d = 0;
    for(i = 0; i < count; i++)
    {
        BC += b[i] * c[i]; 
        AB += a[i] * b[i]; 
        AC += a[i] * c[i];
    }
    d = BC - AB + AC;
    float res1 = 0, res2 = 0, res3 = 0, res = 0;
    for (int i = 0; i < WORK_GROUP_COUNT; i++) {
        res1 += d1[i];
        res2 += d2[i];
        res3 += d3[i];
    }
    res = res1 - res2 + res3;
    
    // Print a brief summary detailing the results
    //
    // printf("Computed '%d/%d' correct values!\n", correct, count);
    printf("CPU d=%f\tGPU d=%f\n", d, res);
    
    // Shutdown and cleanup
    //
    clReleaseMemObject(a_cl);
    clReleaseMemObject(b_cl);
    clReleaseMemObject(c_cl);
    clReleaseMemObject(d1_cl);
    clReleaseMemObject(d2_cl);
    clReleaseMemObject(d3_cl);
    clReleaseProgram(program);
    clReleaseKernel(kernel);
    clReleaseCommandQueue(commands);
    clReleaseContext(context);

    return 0;
}