__kernel void VectorAdd(__global const float* a, __global const float* b, 
						__global float* c, const unsigned int N, 
						__local float* cacheA, __local float* cacheB)
{
    const uint local_id = get_local_id(0);
    const uint global_id = get_global_id(0);
    const uint group_id = get_group_id(0);
    const uint local_size = get_local_size(0);

    cacheA[local_id] = (global_id < N) ? a[global_id] : 0.0f;
	cacheB[local_id] = (global_id < N) ? b[global_id] : 0.0f;
    barrier(CLK_LOCAL_MEM_FENCE);

    for (unsigned int s = local_size >> 1; s > 0; s >>= 1) {
        if (local_id < s) {
            cacheA[local_id] += cacheA[local_id + s];
			cacheB[local_id] += cacheB[local_id + s];
        }
        barrier(CLK_LOCAL_MEM_FENCE);
    }

    if (local_id == 0)
	{ 	
		c[group_id] = cacheA[0]+cacheB[0];
	}
}