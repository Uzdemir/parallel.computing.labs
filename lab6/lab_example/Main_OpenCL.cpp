#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define CL_USE_DEPRECATED_OPENCL_1_2_APIS
//#ifdef __APPLE__
//#include <OpenCL/opencl.h>
//#else
#include <CL/cl.h>
//#endif

#define MEM_SIZE (2048)
#define MAX_SOURCE_SIZE (0x100000)

int main()
{
	cl_device_id device_id = NULL;
	cl_context context = NULL;
	cl_command_queue command_queue = NULL;
	cl_mem memobj = NULL;
	cl_program program = NULL;
	cl_kernel kernel = NULL;
	cl_platform_id platform_id = NULL;
	cl_uint ret_num_devices;
	cl_uint ret_num_platforms;
	cl_int ret;
	cl_int ciErr1 = NULL;
	cl_int ciErr2 = NULL;
	cl_int ciErr3 = NULL;
	cl_int ciErr4 = NULL;
	cl_int memsize = MEM_SIZE;
	/*size_t globalWorkSize = MEM_SIZE;
	size_t localWorkSize = 1;*/

	float *a, *b, *a2, *b2, *c;
	a = new float[MEM_SIZE];
	b = new float[MEM_SIZE];
	c = new float[MEM_SIZE];
	a2 = new float[MEM_SIZE];
	b2 = new float[MEM_SIZE];

	for (int i = 0; i < MEM_SIZE; i++)
	{
		a[i] = i - 500;
		b[i] = i - 500;
	}

	FILE *fp;
	char fileName[] = "./VectorAdd.cl";
	char *source_str;
	size_t source_size;

	/* Load the source code containing the kernel*/
	fp = fopen(fileName, "r");
	if (!fp) {
		fprintf(stderr, "Failed to load kernel.\n");
		exit(1);
	}
	source_str = (char*)malloc(MAX_SOURCE_SIZE);
	source_size = fread(source_str, 1, MAX_SOURCE_SIZE, fp);
	fclose(fp);

	/* Get Platform and Device Info */
	ret = clGetPlatformIDs(1, &platform_id, &ret_num_platforms);
	ret = clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_DEFAULT, 1, &device_id, &ret_num_devices);

	/* Create OpenCL context */
	context = clCreateContext(NULL, 1, &device_id, NULL, NULL, &ret);

	/* Create Command Queue */
	command_queue = clCreateCommandQueue(context, device_id, 0, &ret);

	/* Create Memory Buffer */
	//memobj = clCreateBuffer(context, CL_MEM_READ_WRITE, MEM_SIZE * sizeof(char), NULL, &ret);
	cl_mem cmDevSrcA = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(cl_float) * MEM_SIZE, NULL, &ciErr1);
	cl_mem cmDevSrcB = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(cl_float) * MEM_SIZE, NULL, &ciErr2);
	cl_mem cmDevSrcC = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(cl_float) * MEM_SIZE, NULL, &ciErr3);
	//cl_mem cmDevSIZE = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(cl_int), NULL, &ciErr4);

	/* Create Kernel Program from the source */
	program = clCreateProgramWithSource(context, 1, (const char **)&source_str,
		(const size_t *)&source_size, &ret);

	/* Build Kernel Program */
	ret = clBuildProgram(program, 1, &device_id, NULL, NULL, NULL);


	kernel = clCreateKernel(program, "VectorAdd", &ret);

	ciErr1 = clEnqueueWriteBuffer(command_queue, cmDevSrcA, CL_TRUE, 0, MEM_SIZE * sizeof(cl_float), a, 0, NULL, NULL);
	ciErr2 = clEnqueueWriteBuffer(command_queue, cmDevSrcB, CL_TRUE, 0, MEM_SIZE * sizeof(cl_float), b, 0, NULL, NULL);
	ciErr2 = clEnqueueWriteBuffer(command_queue, cmDevSrcC, CL_FALSE, 0, MEM_SIZE * sizeof(cl_float), c, 0, NULL, NULL);
	//ciErr4 = clEnqueueWriteBuffer(command_queue, cmDevSIZE, CL_FALSE, 0, sizeof(cl_int), &memsize, 0, NULL, NULL);

	/* Set OpenCL Kernel Arguments */
	//ret = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&memobj);
	ciErr1 = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&cmDevSrcA);
	ciErr2 = clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&cmDevSrcB);
	ciErr3 = clSetKernelArg(kernel, 2, sizeof(cl_mem), (void *)&cmDevSrcC);
	ciErr4 = clSetKernelArg(kernel, 3, sizeof(cl_int), (void *)&memsize);


	size_t localSize;
	ret = clGetKernelWorkGroupInfo(kernel, device_id, CL_KERNEL_WORK_GROUP_SIZE, sizeof(localSize), &localSize, NULL);
	if (ret != CL_SUCCESS)
	{
		printf("Error: Failed to retrieve kernel work group info! %d\n", ret);
		exit(1);
	}
	//	size_t globalSize = roundUp(localSize, N); // rounds up to a multiple of localSize
	size_t globalSize = memsize;//ceil(localSize);

	ret = clSetKernelArg(kernel, 4, sizeof(cl_float) * localSize, NULL);
	ret = clSetKernelArg(kernel, 5, sizeof(cl_float) * localSize, NULL);




	/* Execute OpenCL Kernel */
	//ret = clEnqueueTask(command_queue, kernel, 0, NULL, NULL);
	ret = clEnqueueNDRangeKernel(command_queue, kernel, 1, NULL, &globalSize, &localSize, 0, NULL, NULL);

	/* Copy results from the memory buffer */
	//ret = clEnqueueReadBuffer(command_queue, memobj, CL_TRUE, 0, MEM_SIZE * sizeof(char), string, 0, NULL, NULL);
	ret = clEnqueueReadBuffer(command_queue, cmDevSrcC, CL_TRUE, 0, MEM_SIZE * sizeof(cl_float), c, 0, NULL, NULL);

	ret = clEnqueueReadBuffer(command_queue, cmDevSrcA, CL_TRUE, 0, MEM_SIZE * sizeof(cl_float), a2, 0, NULL, NULL);
	ret = clEnqueueReadBuffer(command_queue, cmDevSrcB, CL_TRUE, 0, MEM_SIZE * sizeof(cl_float), b2, 0, NULL, NULL);



	//replace (globalSize/localSize) with the pre-calculated/known number of work groups
	for (int i = 1; i < globalSize / localSize; i++) {
		c[0] += c[i];
	}

	float csumcpu = 0;
	for (int i = 0; i < memsize; i++)
	{
		csumcpu += a[i] + b[i];
	}
	printf("Sum on GPU - %.1f ..... Sum on CPU - %.1f \n", c[0], csumcpu);


	ret = clFlush(command_queue);
	ret = clFinish(command_queue);
	ret = clReleaseKernel(kernel);
	ret = clReleaseProgram(program);
	ret = clReleaseMemObject(memobj);
	ret = clReleaseCommandQueue(command_queue);
	ret = clReleaseContext(context);

	free(source_str);
	system("pause");

}

