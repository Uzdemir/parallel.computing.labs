#define BLOCK_SIZE 1


__kernel void VectorAdd(__global const float* a, __global const float* b, __global float* c, int iNumElements)
{
  // get index into global data array
  int GID = get_global_id(0);
  int LID = get_local_id(0);

  // bound check (equivalent to the limit on a 'for' loop for standard/serial C code
  if (GID >= iNumElements)
  {  
    return; 
  }  

  /*
  __local int la = a[GID];
  __local int lb = b[GID];
  __local int lc = 0;
  //barrier(CLK_LOCAL_MEM_FENCE);
  lc = la + lb;
  //barrier(CLK_LOCAL_MEM_FENCE);
  c[GID] = lc;
  //printf("Hello!!! %d",iGID);
  // add the vector elements
  */
 
  c[GID] = a[GID] + b[GID];


}
