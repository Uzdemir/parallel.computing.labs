__kernel void task_lab(
   __global float* a,
   __global float* b,
   __global float* c,
   __global float* d1,
   __global float* d2,
   __global float* d3,
   const unsigned int count,
   __local float* cache1,
   __local float* cache2,
   __local float* cache3,
   __local float* cache_a,
   __local float* cache_b,
   __local float* cache_c
) 
{
    const uint local_id = get_local_id(0);
    const uint global_id = get_global_id(0);
    const uint group_id = get_group_id(0);
    const uint local_size = get_local_size(0);

    
    cache_a[local_id] = (global_id < count) ? a[global_id] : 0.0f;
	cache_b[local_id] = (global_id < count) ? b[global_id] : 0.0f;
	cache_c[local_id] = (global_id < count) ? c[global_id] : 0.0f;

    barrier(CLK_LOCAL_MEM_FENCE);

    cache1[local_id] = cache_b[local_id] * cache_c[local_id];
    barrier(CLK_LOCAL_MEM_FENCE);
    cache2[local_id] = cache_a[local_id] * cache_b[local_id];
    barrier(CLK_LOCAL_MEM_FENCE);
    cache3[local_id] = cache_a[local_id] * cache_c[local_id];
    barrier(CLK_LOCAL_MEM_FENCE);

    if (local_id == 0) {
        for (int i = 0; i < local_size; i++) {
            d1[group_id] += cache1[i];
            d2[group_id] += cache2[i];
            d3[group_id] += cache3[i];
        }
	}
}