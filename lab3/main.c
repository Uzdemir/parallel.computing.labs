
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <mpi.h>
#include <string.h>

int main(int argc, char* argv[])
{
	int ProcNum, ProcRank, RecvRank, lineCount, pBufferSize, linesPerProc, hardProcCount;
	long long startTime, endTime;
	char **buffer = NULL;
	int *linesSize;
	char *textBuffer = NULL;
	char *pBuffer = NULL;
	const size_t B_SIZE = 256;
	const size_t LINE_SIZE = 256;	
	MPI_Status status;
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &ProcNum);  
	MPI_Comm_rank(MPI_COMM_WORLD, &ProcRank);
	int disp[ProcNum];
	int strSize[ProcNum];
	int textBufferSize = 0;
	const char *SRC = argv[1];
	FILE *resource;
	switch (ProcRank) {
		case 0:
			resource = fopen(SRC, "r");
			if (resource == NULL) {
				printf("ERROR! Can`t open recource %s\n", SRC);
				break;
			}
			lineCount = 0;
			buffer = malloc(sizeof(char*)*B_SIZE);
			buffer[lineCount] = malloc(sizeof(char)*LINE_SIZE);
			while (!feof(resource)) {
				fscanf(resource, "%[^\r\n]%*1[\r\n]", buffer[lineCount]);
				if (lineCount == B_SIZE) {
					break;
				}
				textBufferSize += strlen(buffer[lineCount]);
				// printf("\n%s\n", buffer[lineCount]);
				lineCount++;
				buffer[lineCount] = malloc(sizeof(char)*LINE_SIZE);
			}
			fclose(resource);

			textBuffer = malloc(sizeof(char)*textBufferSize);
			linesSize = malloc(sizeof(int)*lineCount);
			// int o = 0; /*DEBUG*/
			for (int i = 0; i < lineCount; i++) {
				linesSize[i] = strlen(buffer[i]);
				// o+=linesSize[i]; /*DEBUG*/
				// printf("%d\n", linesSize[i]); /*DEBUG*/
				strcat(textBuffer, buffer[i]);
			}
			// printf("\nTotal %d\n", o); /*DEBUG*/
			if (buffer != NULL) {
				for (int i = 0; i < lineCount; i++){
					free(buffer[i]);
				}
				free(buffer);
			}
			hardProcCount = (lineCount%ProcNum);
			linesPerProc = (lineCount - hardProcCount)/ProcNum+1;

			// printf("linePerProc %d\n", linesPerProc); /*DEBUG*/

			int i = 0, p = 0, j = 0;
			disp[0] = 0;
			for (i = 1; i < ProcNum; i++) {
				disp[i] = disp[i-1];
				strSize[i-1] = 0;
				if (i >= hardProcCount) {
					linesPerProc--;
				}
				for (j = p; j < p+linesPerProc; j++) {
					disp[i] += linesSize[j];
					strSize[i-1] += linesSize[j];
				}
				p+=linesPerProc;
			}
			strSize[ProcNum-1] = textBufferSize - disp[ProcNum-1];
			free(linesSize);
	}
	MPI_Bcast(disp, ProcNum, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(strSize, ProcNum, MPI_INT, 0, MPI_COMM_WORLD);
	pBuffer = malloc(strSize[ProcRank]*sizeof(char));
	MPI_Scatterv(textBuffer, strSize, disp, MPI_CHAR, pBuffer, strSize[ProcRank], MPI_CHAR, 0, MPI_COMM_WORLD);
	// printf("\n%s\n", pBuffer);
	char *w;
	char *parser = ",.:;! ";
	char *maxW = "";
	w = strtok(pBuffer, parser);
	do {
		if (strlen(maxW) < strlen(w))
			maxW = w;
		w = strtok(NULL, parser);
	} while(w);

	int maxWSize = strlen(maxW);
	int maxWSizes[ProcNum];
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Gather(&maxWSize, 1, MPI_INT, maxWSizes, 1, MPI_INT, 0, MPI_COMM_WORLD);
	size_t maxWsSTRSize = maxWSizes[0];
	char *maxWs;
	disp[0] = 0;
	if (ProcRank == 0) {
		for (int i = 1; i < ProcNum; i++) {
			maxWsSTRSize += maxWSizes[i];
			disp[i] = disp[i-1] + maxWSizes[i];
		}
		maxWs = malloc(sizeof(char)*maxWsSTRSize);
	}
	MPI_Gatherv(maxW, maxWSize, MPI_CHAR, maxWs, maxWSizes, disp, MPI_CHAR, 0, MPI_COMM_WORLD);

	if (ProcRank == 0) {
		char w[256];
		char maxWord[256];
		w[0] = '\0';
		maxWord[0] = '\0';
		char *wBuf;
		for (int i = 0; i < ProcNum; i++) {
			wBuf = &maxWs[disp[i]];
			strncpy(w, wBuf, maxWSizes[i]);
			if (strlen(maxW) < strlen(w)) {
				// free(maxW);
				// maxW = malloc(sizeof(char)*maxWSizes[i]);
				strcpy(maxW, w);
			}
		}
		printf("\nСамое длинное слово в тексте: %s, символов: %lu\n", maxW, strlen(maxW));
		free(textBuffer);
		free(maxWs);
	}
	free(pBuffer);
	MPI_Finalize();
	return 0; 
} 
